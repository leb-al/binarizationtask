import numpy as np


def _get_y(im):
    return np.tensordot(im.astype(np.float), [0.114, 0.587, 0.299], axes=1)


# def otsu_im(im):
#     y = _get_y(im)
#     DIFFERENT_COLORS = 256
#     (y_hist, _) = np.histogram(y, bins=DIFFERENT_COLORS, range=(0, 255))
#     global e_white, e_black, w_black, w_white
#     e_white = np.sum(y_hist * np.arange(0, DIFFERENT_COLORS))
#     e_black = 0
#     w_black = 0
#     w_white = np.sum(y_hist)
#
#     def update_params(t):
#         global e_white, e_black, w_black, w_white
#         cur_y_count = y_hist[t]
#         e_white -= cur_y_count * t
#         e_black += cur_y_count * t
#         w_black += cur_y_count
#         w_white -= cur_y_count
#         return np.uint64(w_white) * w_black * np.power(e_white - e_black, 2)
#
#     vars = [update_params(i) for i in range(DIFFERENT_COLORS)]
#     threshold = np.argmax(vars)
#
#     result = np.zeros(y.shape, np.uint8)
#     result[y > threshold] = 255
#     return result


def folding(matrix, w):
    def inner_fun(row):
        ac = np.add.accumulate(row)
        # в разнице не учитывается само изначальное слагаемое, поэтому прибавим его
        return (ac[w - 1:] - ac[:1 - w] + row[:1 - w]) / w

    return np.array([inner_fun(r) for r in matrix])


def box_flter_3d(im, w, h):
    im = im.transpose((2, 0, 1))
    folder_row = [folding(c, w) for c in im]
    return np.transpose([folding(c.transpose(), h) for c in folder_row], (2, 1, 0)).astype(np.uint8)


def box_flter_2d(im, w, h):
    return np.transpose(folding(folding(im, h).transpose(), w))


def adaptive_binarisation_small_part(imy, max_std, miny):
    # # Niblack
    # threshold = np.mean(imy) - 0.2 * np.std(imy)
    # # Sauvola
    # threshold = np.mean(imy) * (1 - 0.5 * (1 - np.std(imy) / 128))
    # # Sauvola+
    # threshold = np.mean(imy) * (1 - 0.5 * (1 - np.std(imy) / dynamic_range / 50))

    m = np.mean(imy)
    s = np.std(imy)
    # Неудачные формулы из статьи
    # threshold = (1 - ALPHA1) * m + K1 * np.power(ratio, GAMMA + 1) * (m - miny) + K2 * np.power(ratio, GAMMA) * miny
    # threshold = (1 - k) * m + k * (miny + np.power(s / max_std, 1) * (m - miny))


    k = 0.5
    max_std += 1e-10
    threshold = (1 - k) * m + k * (miny + s / max_std * (m - miny))
    return np.ones(imy.shape) * threshold


def adaptive_binarisation_huge_part(imy_original, huge_size, small_size, parent_white_on_black):
    imy = np.array(imy_original)
    assert imy.shape[0] % small_size == 0
    assert imy.shape[1] % small_size == 0

    # Детектор цвета фона
    white_on_black_count = 0
    if np.median(imy) < np.mean(imy):
        white_on_black_count += 1
    if np.median(imy) < np.mean(imy) * 0.95:
        white_on_black_count += 1
    if np.mean(imy) < ((np.max(imy) + np.min(imy)) / 2.0):
        white_on_black_count += 1
    if parent_white_on_black:
        white_on_black_count += 1
    white_on_black = white_on_black_count > 2 and (huge_size > 64 or parent_white_on_black)

    if white_on_black:
        imy = -imy + 255.0

    countx = imy.shape[0] // small_size
    county = imy.shape[1] // small_size
    assert countx == 4
    assert county == 4

    def iterate_local_windows(fun):
        def fun_for_window(x, y):
            start_x = x * small_size
            start_y = y * small_size
            end_x = start_x + small_size
            end_y = start_y + small_size
            window = imy[start_x:end_x, start_y:end_y]
            fun(window, x, y, start_x, end_x, start_y, end_y)

        for x in range(countx):
            for y in range(county):
                fun_for_window(x, y)

    vars = np.zeros((countx, county))

    def var_fun(m, x, y, start_x, end_x, start_y, end_y):
        vars[x, y] = np.std(m)

    iterate_local_windows(var_fun)

    # dynamic_range = np.log1p(np.max(vars)) - np.log1p(np.min(vars))

    max_std = np.max(vars)

    result = None
    # if max_std > 6 * np.min(vars) and small_size > 16:
    if max_std > 2.1 * np.std(imy) and small_size > 16:
        huge_size //= 2
        small_size //= 2
        result = binarization_part(imy_original, huge_size, small_size, white_on_black)
        assert max_std > 0

    else:
        result = np.zeros(imy.shape)

        def local_window_fun(m, x, y_, start_x, end_x, start_y, end_y):
            result[start_x:end_x, start_y:end_y] = adaptive_binarisation_small_part(m, max_std, np.min(imy))

        iterate_local_windows(local_window_fun)

    if white_on_black:
        result = result - 255

    return result


def binarization_part(imy, huge_size, small_size, parent_white_on_black):
    result = np.zeros(imy.shape)
    countx = imy.shape[0] // huge_size
    county = imy.shape[1] // huge_size

    last_index_modification = 1 if imy.shape[0] % huge_size == 0 and imy.shape[1] % huge_size == 0 else 0

    for x in range(countx - last_index_modification):
        result[x * huge_size: x * huge_size + huge_size, -huge_size:] = adaptive_binarisation_huge_part(
            imy[x * huge_size: x * huge_size + huge_size, -huge_size:], huge_size, small_size, parent_white_on_black)
    for y in range(county - last_index_modification):
        result[-huge_size:, y * huge_size: y * huge_size + huge_size] = adaptive_binarisation_huge_part(
            imy[-huge_size:, y * huge_size: y * huge_size + huge_size], huge_size, small_size, parent_white_on_black)
    result[-huge_size:, -huge_size:] = adaptive_binarisation_huge_part(
        imy[-huge_size:, -huge_size:], huge_size, small_size, parent_white_on_black)
    for x in range(countx - last_index_modification):
        for y in range(county - last_index_modification):
            result[x * huge_size: x * huge_size + huge_size, y * huge_size: y * huge_size + huge_size:] = adaptive_binarisation_huge_part(
                imy[x * huge_size: x * huge_size + huge_size, y * huge_size: y * huge_size + huge_size], huge_size, small_size, parent_white_on_black)
    return result


def adaptive_binarisation(im):
    small_size = 256
    huge_size = 4 * small_size

    # im[1:-1, 1:-1] = box_flter_3d(im, 3, 3)
    imy = _get_y(im)

    result = binarization_part(imy, huge_size, small_size, False)
    result_unsigned = np.abs(result)
    result_sing = result / result_unsigned

    result_smooth = box_flter_2d(result_unsigned, small_size, small_size)
    result_unsigned[small_size // 2: 1 - small_size // 2, small_size // 2: 1 - small_size // 2] = result_smooth

    binarized = (imy * result_sing > result_unsigned * result_sing)
    binarized ^= (result_sing < -0.01)

    return np.array(binarized * 255, np.uint8)

    # отладка детектора цвета фона
    res = np.transpose([np.array(binarized * 255, np.uint8),
                        (result_sing < -0.01) * 255,
                        (result_sing > 0.01) * 255], (1, 2, 0))
    return res


def binarize(im):
    return adaptive_binarisation(im)
