import os

import cv2

from binarizer import binarize

INPUT_FOLDER = 'input/'
OUPUT_FOLDER = 'output/'


def read_input():
    files = os.listdir(INPUT_FOLDER)

    result = []
    for f in files:
        path = INPUT_FOLDER + f
        im = cv2.imread(path)
        result.append((im, path))
    return result


def write_images(images):
    for (im, path) in images:
        path = path.replace(INPUT_FOLDER, OUPUT_FOLDER)
        cv2.imwrite(path, im)
        print(path)

def binarize_one_image(im, path):
    print(path)
    im = binarize(im)
    return (im, path)

def main():
    images = read_input()
    images = [binarize_one_image(im, path) for (im, path) in images]
    write_images(images)


if __name__ == "__main__":
    main()
